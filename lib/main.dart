import 'package:flutter/material.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: CulqiDemoApp(),
    );
  }
}

class CulqiDemoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Demo'),
      ),
      body: RaisedButton(
        child: Text('Launch CulqiButton'),
        onPressed: () async {
          final result = await FlutterWebBrowser.openWebPage(
            url: "https://fir-8ceb5.firebaseapp.com/culqi.html#12345",
            customTabsOptions: CustomTabsOptions(
              colorScheme: CustomTabsColorScheme.dark,
              toolbarColor: Colors.deepPurple,
              secondaryToolbarColor: Colors.green,
              navigationBarColor: Colors.amber,
              addDefaultShareMenuItem: true,
              instantAppsEnabled: true,
              showTitle: false,
              urlBarHidingEnabled: true,
            ),
            safariVCOptions: SafariViewControllerOptions(
              barCollapsingEnabled: true,
              preferredBarTintColor: Colors.green,
              preferredControlTintColor: Colors.amber,
              dismissButtonStyle: SafariViewControllerDismissButtonStyle.close,
              modalPresentationCapturesStatusBarAppearance: true,
            ),
          );
          print(result);
        },
      ),
    );
  }
}
